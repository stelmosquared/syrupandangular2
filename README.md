###THIS TUTORIAL IS BASED ON TUTSPLUS ANGULAR 2 WALKTHROUGH###

FILES CREATED:
  tsconfig.json				// this is the TypeScript compiler configuration file. It guides the compiler to generate JavaScript files. 
  tpings.json 				// this is used to identify TypeScript definition fies in Anguar App
 					//in this app we are using 3 typing fies core-js, jasmine and node
  package.json  			// contains the packages that our app requires - uses npm to instal (run 'npm install'

  app/    				//this subfolder houses angular app components
  /app/enviroment_app.component.ts  	//import the Component and View package from angular2/core.
				    	//The @Component is an Angular 2 decorator that allows you to associate metadata with the component class.
				    	//The my-app can be used as HTML tag to injecting and can be used as a component.
				    	//The @view contains a template that tells Angular how to render a view.
				    	//The export specifies that, this component will be available outside the file.

  /app/environment_main.ts  		//tells Angular to load the component
					//to launch app, we need to import browser bootstrap function and root component of the application
					//after importing, the bootstrap is caed by passing the root compent type 

  index.html				//Angular will launch the app in the browser with our componets and places it in a specific location on index.html
  